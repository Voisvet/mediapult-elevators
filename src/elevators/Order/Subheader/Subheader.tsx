import * as React from 'react';

import {
  ActiveSortingButton,
  City,
  CityContainer, NextButton, Offers, SortingButton,
  SortingContainer, SortingLabel,
  SubheaderContainer,
  TotalContainer, TotalTextContainer
} from 'elevators/Order/Subheader/Subheader.styles';

class Subheader extends React.Component<any, any> {
  getFilteringButtons = () => {
    switch (this.props.filteringOrder) {
      case 'price':
        return (
            <React.Fragment>
              <ActiveSortingButton onClick={() => this.props.onFilteringOrderChange('price')}>Цена<hr/></ActiveSortingButton>
              <SortingButton onClick={() => this.props.onFilteringOrderChange('audience')}>Аудитория</SortingButton>
            </React.Fragment>
        );
      case 'audience':
        return (
            <React.Fragment>
              <SortingButton onClick={() => this.props.onFilteringOrderChange('price')}>Цена</SortingButton>
              <ActiveSortingButton onClick={() => this.props.onFilteringOrderChange('audience')}>Аудитория<hr/></ActiveSortingButton>
            </React.Fragment>
        );
      default:
        return (
            <React.Fragment>
              <ActiveSortingButton onClick={() => this.props.onFilteringOrderChange('price')}>Цена<hr/></ActiveSortingButton>
              <SortingButton onClick={() => this.props.onFilteringOrderChange('audience')}>Аудитория</SortingButton>
            </React.Fragment>
        );
    }
  };

  render() {
    return (
      <SubheaderContainer>
        <CityContainer>Город: <City>Хабаровск</City> <Offers>| {this.props.amountOfOffers} предложения</Offers></CityContainer>
        <SortingContainer>
          <SortingLabel>Сортировка:</SortingLabel>
          {this.getFilteringButtons()}
        </SortingContainer>
        <TotalContainer>
          <TotalTextContainer>
            <p>выбрано пакетов: {this.props.offersChosen}</p>
            <p>аудитория {this.props.totalAudience} чел.</p>
          </TotalTextContainer>
          <TotalTextContainer style={{marginRight: '32px'}}>
            <p>цена:</p>
            <p><span style={{fontSize: '18px', fontWeight: 700}}>{this.props.totalPrice}</span> руб.</p>
          </TotalTextContainer>
          <NextButton>Далее</NextButton>
        </TotalContainer>
      </SubheaderContainer>
    );
  }
}

export default Subheader;
