import * as styled from 'styled-components';

export const SubheaderContainer = styled.div`
    flex: 0 0 50px;
    background-color: #ffffff;
    display: flex;
    padding: 16px 24px;
    align-items: center;
    justify-content: space-between;
`;

export const CityContainer = styled.div`
    font-weight: 500;
    flex: 1 0 auto;
`;

export const City = styled.span`
    font-weight: 900;
`;

export const Offers = styled.span`
    color: #ccc;
    font-size: 14px;
`;

export const SortingContainer = styled.div`
    flex: 1 0 auto;
    display: flex;
    justify-content: center;
    align-items: center;
`;

export const SortingLabel = styled.h4`
    text-transform: uppercase;
    color: #aaa;
    font-size: 14px;
    line-height: 14px;
    font-weight: 700;
    margin-right: 24px;
`;

export const SortingButton = styled.button`
    background: none;
    border: none;
    padding: 8px 16px;
    font-size: 14px;
    font-weight: 700;
    color: #aaa;
    cursor: pointer;
    margin: 0 8px;
`;

export const ActiveSortingButton = styled(SortingButton)`
    color: #555;
    
    hr {
        background-color: #1a91eb;
        border: none;
        position: relative;
        top: 26px;
        margin: 0 -8px;
        height: 4px;
    }
`;

export const TotalContainer = styled.div`
    flex: 1 0 auto;
    display: flex;
    justify-content: right;
`;

export const TotalTextContainer = styled.div`
    display: flex;
    justify-content: space-around;
    flex-direction: column;
    font-size: 12px;
    font-weight: 500;
    color: #ff7900;
    margin-right: 16px;
`;

export const NextButton = styled.button`
    cursor: pointer;
    border-radius: 4px;
    background-color: #34ab45;
    border: 1px solid #259533;
    color: #ffffff;
    font-size: 18px;
    font-weight: 500;
    padding: 8px 32px;
    
    &:hover, &:active {
        background-color: #4ac85c;
        border-color: #2fbe41;
    }
`;
