import * as React from 'react';
import Content from './Content/Content';
import Header from './Header/Header';
import Subheader from './Subheader/Subheader';
import Footer from '../Landing/Footer/ContactsBlock/ContactsBlock';
// import Footer from './Footer/Footer';

import { OrderContainer } from 'elevators/Order/Order.styles';

class Order extends React.Component<any, any> {
    state = {
        offers: [
            {
                name: 'Эгершельд',
                price: 20990,
                audience: 24000,
                format: 'A4',
                count: 24,
                company: 'Хабаровск-лифт',
                address: 'улица Пушкина',
                discount: 1000,
                inCart: false
            },
            {
                name: 'Дельшегер',
                price: 10041,
                audience: 26000,
                format: 'A10',
                count: 11,
                company: 'Феличита-лифт',
                address: 'улица Академика Попова',
                discount: 2000,
                inCart: false
            },
            {
                name: 'Иннополис',
                price: 5000,
                audience: 1000,
                format: 'A10',
                count: 1,
                company: 'Лифты Иннополиса',
                address: 'Университет Иннополис',
                discount: 2000,
                inCart: true
            }
        ],

        filteringOrder: 'price'
    };

    getOffers = () => {
        return this.state.offers.sort((a, b) => (
            this.state.filteringOrder === 'price' ?
                a.price - b.price :
                a.audience - b.audience
        ));
    };

    getTotalPrice = () => {
        return this.state.offers.filter(item => item.inCart)
            .map(item => item.price)
            .reduce((sum, current) => sum + current, 0);
    };

    getTotalAudience = () => {
        return this.state.offers.filter(item => item.inCart)
            .map(item => item.audience)
            .reduce((sum, current) => sum + current, 0);
    };

    render(): React.ReactNode {

        return (
            <OrderContainer>
                <Header/>
                <Subheader
                    onFilteringOrderChange={type => this.setState({filteringOrder: type})}
                    filteringOrder={this.state.filteringOrder}
                    amountOfOffers={this.state.offers.length}
                    totalPrice={this.getTotalPrice()}
                    totalAudience={this.getTotalAudience()}
                    offersChosen={this.state.offers.filter(item => item.inCart).length}
                />
                <Content offers={this.getOffers()}/>
                <Footer/>
            </OrderContainer>
        );
    }
}

export default Order;
