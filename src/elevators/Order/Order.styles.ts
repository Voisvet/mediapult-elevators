import * as styled from 'styled-components';

export const OrderContainer = styled.div`
    display: flex;
    flex-direction: column;
    min-height: 100%;
`;
