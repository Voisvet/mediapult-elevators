import * as styled from 'styled-components';

export const FiltersWrapper = styled.div`
    width: 400px;
    height: 470px;
    flex: 0 0 auto;
    padding: 16px;
    background-color: #FFFFFF;
    border-radius: 0.1em;
    box-shadow: rgba(0, 0, 0, 0.12) 0px 1px 3px, rgba(0, 0, 0, 0.24) 0px 1px 2px;
`;

export const Parameter = styled.div`
    margin-top: 24px;
    
    &:first-child {
        margin-top: 0;
    }
    
    select {
        width: 100%;
    }
    
    p {
        text-transform: uppercase;
        margin-bottom: 8px;
        font-weight: bold;
        font-size: medium;
        color: #888;
    }
    
    span {
        display: block;
        
        input {
            margin-right: 8px;
        }
    }
`;

export const ButtonContainer = styled.div`
    width: 100%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    margin-top: 40px;
`;

export const ClearButton = styled.button`
    border: none;
    background: none;
    color: #2ab7ef;
    text-transform: uppercase;
    cursor: pointer;
    font-weight: bold;
    font-size: medium;
`;
