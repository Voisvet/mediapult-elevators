import * as React from 'react';

import { FiltersWrapper, Parameter, ClearButton, ButtonContainer } from './FiltersContainer.styles';
import Select from 'elevators/components/select';

import { Range } from 'elevators/components/Range/Range';

class FiltersContainer extends React.Component<any, any> {
    constructor(props: any) {
        super(props);
    }

    render() {
        return (
            <FiltersWrapper>
                <Parameter>
                    <p>Район</p>
                    <Select
                        options={this.props.districts}
                        onChange={(district) => this.props.onFiltersChange('district', district)}
                        value={this.props.filters.district}
                    />
                </Parameter>
                <Parameter>
                    <p>Форматы</p>
                    {this.props.formats.map((format, ind) => (
                        <span key={ind}><input type="checkbox" name="format{ind}" value="{format}" />{format}</span>
                    ))}
                </Parameter>
                <Parameter>
                    <p>Аудитория</p>
                    <Range
                        min={this.props.audience.min}
                        max={this.props.audience.max}
                        value={this.props.audience.value}
                        onAfterChange={(values) => this.props.onFiltersChange('audience', values)}
                    />
                </Parameter>
                <Parameter>
                    <p>Цена</p>
                    <Range
                        min={this.props.price.min}
                        max={this.props.price.max}
                        value={this.props.price.value}
                        onAfterChange={(values) => this.props.onFiltersChange('price', values)}
                    />
                </Parameter>
                <ButtonContainer>
                    <ClearButton onClick={this.props.clearFilters}>Сбросить все фильтры</ClearButton>
                </ButtonContainer>
            </FiltersWrapper>
        );
    }
}

export default FiltersContainer;
