import * as React from 'react';
import { BreadCrumbsContainer } from 'elevators/Order/Content/Breadcrumbs/Breadcrumbs.styles';

class Breadcrumbs extends React.Component {
    render() {
        return (
            <BreadCrumbsContainer>
                Главная » Реклама в лифтах » <span>Пакеты</span>
            </BreadCrumbsContainer>
        );
    }
}

export default Breadcrumbs;
