import * as styled from 'styled-components';

export const BreadCrumbsContainer = styled.div`
    display: flex;
    margin-bottom: 16px;
    font-size: 14px;
    font-weight: 500;
    color: #aaa;
    
    span {
        color: #555;
        margin-left: 4px;
    }
`;
