import * as styled from 'styled-components';

export const ContentContainer = styled.div`
    display: flex;
    flex-direction: column;
    flex: 1 0 auto;
    background-color: #eff3f6;
    padding: 16px 24px;
`;

export const OrderFormContainer = styled.div`
    display: flex;
`;

