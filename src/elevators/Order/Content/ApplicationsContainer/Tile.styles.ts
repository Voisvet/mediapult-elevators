import * as styled from 'styled-components';

export const TileBox = styled.div`
  width: 27%;
  box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
  border-radius: 0.1em;
  transition: all 0.3s cubic-bezier(.25,.8,.25,1);
  margin: 1em;
  background: white;
  
  
	transition: transform 0.5s;

	&::after {
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
		transition: opacity 2s cubic-bezier(0.165, 0.84, 0.44, 1);
		box-shadow: 0 8px 17px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.15);
		content: "";
		opacity: 0;
		z-index: -1;
	}

	&:hover,
	&:focus {
		transform: scale3d(1.006, 1.006, 1);

		&::after {
			opacity: 1;
		}
	}

`;

export const AudienceMeasurement = styled.span`
  color: #222c3c;
  font-weight: 100;
`;

export const Description = styled.div`
  color: #848c98;
`;

export const Db = styled.span`
  color: #222c3c;
  font-weight: 400;
`;

export const TileBottom = styled.div`
  padding: 1em 1em 0 1em;
`;

export const TileButtons = styled.div`
  display:flex;
  flex-flow: row;
  justify-content: space-between;
  align-items: center;
  padding 1em 1em 1em 1em;
`;

const BottomButton = styled.div`
  cursor: pointer;
  padding: 0.3em;
  border: solid;
  border-radius: 0.3em;
  border-width: 0.08em;
  margin: 0 0.5em 0 0.5em;
`;

export const DeleteBtn = styled(BottomButton)`
  color: #ff7800;
  border-color: #ff7800;
`;

export const AddressesGray = styled(BottomButton)`
  color: #ced0da;
  border-color: #ced0da;
`;

export const AddressesBlue = styled(BottomButton)`
  color: #2ea2f8;
  border-color: #2ea2f8;

`;
export const AddToCartOrange = styled(BottomButton)`
  color: white;
  border-color: #ff7800;
  background: #ff7800;
`;

export const IconImage = styled.img`
  height: 0.7em;
  margin-right: 0.1em;
`;
