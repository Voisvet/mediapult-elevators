import * as styled from 'styled-components';

export const Table = styled.table`
    width: 100%;
    border: 1px solid #e6eaee;
    border-collapse: collapse;
    padding-left: 1em;
    background: white;
`;

export const TableHead = styled.thead`
    background:#f5f8fa;
    color: rgba(127, 143, 164, 255);
    text-align: left;

   `;
export const Th = styled.th`
    font-weight: 400;
    font-size: 0.9em;
    padding: 0.5em; 
    `;

export const ThFirst = styled(Th)` 
    padding-left: 1em;
`;
