import * as styled from 'styled-components';

export const Address = styled.div`
    color: #7f8fa4;
    font-weight: 100;
    font-size: 0.79em;
`;

export const Td = styled.td`
    color: #333c48;
    font-weight: 400;
    font-size:0.8em;
    padding: 0.7em 0.7em 0.7em 0.4em;
`;

export const TdFirst = styled.td`
    color: #333c48;
    font-weight: 400;
    font-size:0.8em;
    padding: 0.7em 0.7em 0.7em 0.8em;
`;

export const TdFirstEven = styled.td`
    border-left: 5px solid #1a91eb;
    color: #333c48;
    font-weight: 400;
    font-size:0.8em;
    padding: 0.7em 0.7em 0.7em 0.8em;
`;

export const Discount = styled.div`
    color: #7f8fa4;
    font-weight: 100;
    font-size: 0.79em;
`;

export const Tr = styled.tr`
    border-bottom: 1px solid #e7e9ed;
`;

export const ClickableImg = styled.img`
    cursor: pointer;
    width: 35;
    height: 35;
`;
