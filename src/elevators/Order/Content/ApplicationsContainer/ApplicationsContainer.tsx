import * as React from 'react';
import {
    AppsContainer,
    AppsWrapper,
    BottomButtons,
    LoadMoreBtn,
    SelectorImage,
    SelectorImageClickable,
    TopButtons
} from './ApplicationsContainer.styles';
import ApplicationsList, { Item, ListTypes } from './ApplicationsList';
import Tile, { TileStatuses } from 'elevators/Order/Content/ApplicationsContainer/Tile';

const gridIconClickable = require('../../../../assets/grid_icon_gray.png');
const gridIconActive = require('../../../../assets/grid_icon_blue.png');
const listIconClickable = require('../../../../assets/list_icon_gray.png');
const listIconActive = require('../../../../assets/list_icon_blue.png');

const divider = require('../../../../assets/divider.png');

interface AcState {
    type: ListTypes;
    items: Item[];
}

interface AcProps {
    items: Item[];
}

class ApplicationsContainer extends React.Component<AcProps, AcState> {

    constructor(props: any) {
        super(props);
        this.state = {
            type: ListTypes.TILES,
            items: ApplicationsContainer.generateItems()
        };

        this.updateType = this.updateType.bind(this);
        this.loadMore = this.loadMore.bind(this);
    }

    updateType() {
        switch (this.state.type) {
            case ListTypes.TABLE:
                this.setState({type: ListTypes.TILES});
                break;
            case ListTypes.TILES:
                this.setState({type: ListTypes.TABLE});
                break;
            default:
                break;
        }
    }

    loadMore() {
        this.setState({items: this.state.items.concat(ApplicationsContainer.generateItems())});
    }

    render() {
        return (
            <AppsContainer>
                <AppsWrapper>
                    <TopButtons>
                        <div onClick={this.updateType}>
                            <SelectorImageClickable
                                src={this.state.type === ListTypes.TILES ? gridIconActive : gridIconClickable}
                                alt="Grid view button"
                                width={14}
                                height={14}

                            />
                        </div>
                        <SelectorImage
                            src={divider}
                            alt="Divider"
                            width={1}
                            height={20}
                        />
                        <div onClick={this.updateType}>
                            <SelectorImageClickable
                                src={this.state.type === ListTypes.TABLE ? listIconActive : listIconClickable}
                                alt="List view button"
                                width={16}
                                height={14}
                            />
                        </div>
                    </TopButtons>
                    <ApplicationsList type={this.state.type} items={this.props.items}/>
                    <BottomButtons>
                        <LoadMoreBtn onClick={this.loadMore}>ПОКАЗАТЬ ЕЩЁ</LoadMoreBtn>
                    </BottomButtons>
                </AppsWrapper>
            </AppsContainer>
        );
    }

    static generateItems(): Item[] {
        const it: Item = {
            name: 'Эгершельд',
            price: 20990,
            audience: 24000,
            format: 'A4',
            count: 24,
            company: 'Хабаровск-лифт',
            address: 'улица Пушкина',
            discount: 1000,
            inCart: false
        };

        const it1: Item = {
            name: 'Дельшегер',
            price: 10041,
            audience: 14000,
            format: 'A10',
            count: 11,
            company: 'Феличита-лифт',
            address: 'улица Академика Попова',
            discount: 2000,
            inCart: true
        };

        return [Math.random() >= 0.5 ? it : it1,
            Math.random() >= 0.5 ? it : it1,
            Math.random() >= 0.5 ? it : it1];
    }
}

export default ApplicationsContainer;
