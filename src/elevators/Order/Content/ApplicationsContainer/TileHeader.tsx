import * as React from 'react';
import {
    Name, NameWhite,
    Price,
    PriceSubText, PriceSubTextWhite, PriceWhite,
    TileHeaderWrapper, TileHeaderWrapperBlue
} from 'elevators/Order/Content/ApplicationsContainer/TileHeader.styles';
import { TileStatuses } from 'elevators/Order/Content/ApplicationsContainer/Tile';

interface TileHeaderProps {
    name: string;
    price: string;
    priceSubText: string;
    status: TileStatuses;
}

interface TileHeaderState {

}

class TileHeader extends React.Component<TileHeaderProps, TileHeaderState> {
    render() {
        if (this.props.status === TileStatuses.IN_CART) {
            return (
                <TileHeaderWrapperBlue>
                    <NameWhite>
                        {this.props.name}
                    </NameWhite>
                    <PriceWhite>
                        {this.props.price}
                    </PriceWhite>
                    <PriceSubTextWhite>
                        стоимость руб/мес.
                    </PriceSubTextWhite>
                </TileHeaderWrapperBlue>
            );

        } else {
            return (
                <TileHeaderWrapper>
                    <Name>
                        {this.props.name}
                    </Name>
                    <Price>
                        {this.props.price}
                    </Price>
                    <PriceSubText>
                        стоимость руб/мес.
                    </PriceSubText>
                </TileHeaderWrapper>
            );
        }
    }
}

export default TileHeader;
