import * as React from 'react';
import Tile, { TileStatuses } from './Tile';
import { TilesContainer } from './ApplicationTiles.styles';
import { Item } from 'elevators/Order/Content/ApplicationsContainer/ApplicationsList';

interface AtProps {
    items: Item[];
}

interface AtState {

}

class ApplicationsTiles extends React.Component<AtProps, AtState> {
    render() {
        return (
            <TilesContainer>
                {this.props.items.map((item, id) => (<Tile
                    name={item.name}
                    count={item.count}
                    price={item.price.toString()}
                    audience={item.audience}
                    format={item.format}
                    company={item.company}
                    status={item.inCart ? TileStatuses.IN_CART : TileStatuses.NOT_IN_CART}
                />))}
            </TilesContainer>
        );
    }
}

export default ApplicationsTiles;
