import * as styled from 'styled-components';

export const AppsContainer = styled.div`
    width: 100%;
`;

export const AppsWrapper = styled.div`
    padding: 1em;
`;

export const TopButtons = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: flex-end;
    align-items: center;
    padding: 0.5em 0 0.5em 0;
`;

export const SelectorImageClickable = styled.img`
    cursor: pointer;
    margin-right:0.6em;
`;
export const SelectorImage = styled.img`
    margin-right:0.6em;
`;

export const BottomButtons = styled.div`
    display: flex;
    justify-content: center;
    margin-top: 1em;
`;

export const LoadMoreBtn = styled.div`
  font-size: 1em;
  font-weight: 400;
  padding: 0.4em;
  cursor: pointer;
  color: #00aeef;
  text-decoration: none;
  border: solid;
  border-radius: 0.5em;
  border-color: #00aeef;
  border-width: 0.1em;
`;
