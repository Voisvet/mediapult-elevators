import * as React from 'react';
import {
    AddressesBlue,
    AddressesGray,
    AddToCartOrange,
    AudienceMeasurement,
    Db,
    DeleteBtn,
    Description, IconImage,
    TileBottom,
    TileBox,
    TileButtons,
} from './Tile.styles';

import TileHeader from 'elevators/Order/Content/ApplicationsContainer/TileHeader';

const addIcon = require('../../../../assets/icon-plus-white.svg');
const deleteIcon = require('../../../../assets/icon-cross-orange.svg');
const locationIconBlue = require('../../../../assets/icon-location-blue.svg');
const locationIconGray = require('../../../../assets/icon-location-gray.svg');

interface TileState {
}

interface TileProps {
    name: string;
    price: string;
    audience: number;
    format: string;
    company: string;
    count: number;
    status: TileStatuses;
}

class Tile extends React.Component<TileProps, TileState> {
    render() {

        let tileButtons;

        if (this.props.status === TileStatuses.IN_CART) {
            tileButtons = (
                <TileButtons>
                    <AddressesGray><IconImage src={locationIconGray} alt="location"/>Адреса</AddressesGray>
                    <DeleteBtn>
                        <IconImage src={deleteIcon} alt="delete"/>Удалить
                    </DeleteBtn>
                </TileButtons>);
        } else {
            tileButtons = (
                <TileButtons>
                    <AddressesBlue><IconImage src={locationIconBlue} alt="address"/>Адреса</AddressesBlue>
                    <AddToCartOrange><IconImage src={addIcon} alt="add to cart"/>В корзину</AddToCartOrange>
                </TileButtons>);

        }
        return (
            <TileBox>
                <TileHeader
                    name={this.props.name}
                    price={this.props.price}
                    priceSubText={'стоимость руб/мес'}
                    status={this.props.status}
                />
                <TileBottom>
                    <Description>
                        Аудитория: <Db>{this.props.audience}</Db> <AudienceMeasurement>чел.</AudienceMeasurement>
                    </Description>
                    <Description>
                        Формат: <Db>{this.props.format}</Db>
                    </Description>
                    <Description>
                        Лифты: <Db>{this.props.count}</Db>
                    </Description>
                    <Description>
                        Компания: <Db>{this.props.company}</Db>
                    </Description>
                </TileBottom>
                {tileButtons}
            </TileBox>
        );
    }
}

export enum TileStatuses {
    IN_CART,
    NOT_IN_CART
}

export default Tile;
