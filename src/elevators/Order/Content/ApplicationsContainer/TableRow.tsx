import * as React from 'react';
import { Address, ClickableImg, Discount, Td, TdFirst, TdFirstEven, Tr } from './TableRow.styles';

const addButton = require('../../../../assets/app_table_row_add_btn.png');
const deleteButton = require('../../../../assets/app_table_row_delete_btn.png');

export enum ButtonActions {
    ADD_TO_CART,
    DELETE_FROM_CART
}

interface TableRowProps {
    name: string;
    address: string;
    count: number;
    price: number;
    discount: number;
    audience: number;
    format: string;
    company: string;
    inCart: boolean;
}

interface TableRowState {

}

class TableRow extends React.Component<TableRowProps, TableRowState> {
    render() {
        let button;
        let first;
        if (!this.props.inCart) {
            first = <TdFirst>{this.props.name} <Address>{this.props.address}</Address></TdFirst>;
            button = <ClickableImg src={addButton} alt={'Add to cart'} />;
        } else {
            first = <TdFirstEven>{this.props.name} <Address>{this.props.address}</Address></TdFirstEven>;
            button = <ClickableImg src={deleteButton} alt={'Deletion from cart'} />;
        }
        return (
            <Tr>
                {first}
                <Td>{this.props.count}</Td>
                <Td>{this.props.audience} чел.</Td>
                <Td>{this.props.format}</Td>
                <Td>{this.props.company}</Td>
                <Td>{this.props.price} руб. <br/> <Discount>Скидка {this.props.discount} руб.</Discount> </Td>
                <Td>{button}</Td>
            </Tr>
        );
    }
}

export default TableRow;
