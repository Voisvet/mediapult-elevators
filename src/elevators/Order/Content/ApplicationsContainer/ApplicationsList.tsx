import * as React from 'react';
import ApplicationsTable from './ApplicationsTable';
import ApplicationsTiles from './ApplicationsTiles';
import { ButtonActions } from 'elevators/Order/Content/ApplicationsContainer/TableRow';

export enum ListTypes {
    TABLE,
    TILES
}

interface AlState {
    type: ListTypes;
}

interface AlProps {
    type: ListTypes;
    items: Item[];
}

class ApplicationsList extends React.Component<AlProps, AlState> {

    constructor(props: AlProps) {
        super(props);
        this.state = {type: this.props.type};
    }

    componentWillReceiveProps(nextProps: Readonly<AlProps>, nextContext: any): void {
        this.setState({type: nextProps.type});
    }

    render() {
        switch (this.state.type) {
            case ListTypes.TABLE:
                return <ApplicationsTable items={this.props.items}/>;
            case ListTypes.TILES:
            default:
                return <ApplicationsTiles items={this.props.items}/>;
        }
    }
}

export interface Item {
    name: string;
    address: string;
    count: number;
    price: number;
    discount: number;
    audience: number;
    format: string;
    company: string;
    inCart: boolean;
}
export default ApplicationsList;
