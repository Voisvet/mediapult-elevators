import * as styled from 'styled-components';

export const TileHeaderWrapper = styled.div`
  display: flex;
  flex-flow: column;
  align-items: center;  padding: 1em 1em 0 1em;
  border-bottom: solid;
  border-bottom-color: #e6eaee;
  border-bottom-width: thin;
`;

export const TileHeaderWrapperBlue = styled.div`
  display: flex;
  flex-flow: column;
  align-items: center;  padding: 1em 1em 0 1em;
  border-bottom: solid;
  border-bottom-color: #e6eaee;
  border-bottom-width: thin;
  background: #00aeef;
`;

export const Price = styled.div`
  color: #00aeef;
  font-size: 2.5em;
  font-weight: 400;
`;

export const Name = styled.div`
  font-weight: 400;
`;

export const PriceSubText = styled.div`
  color: #848c98;
  margin-bottom: 1em;
  font-size: 0.8em;
`;

export const PriceWhite = styled.div`
  color: white;
  font-size: 2.5em;
  font-weight: 400;
`;

export const NameWhite = styled.div`
  font-weight: 400;
  color: white;
`;

export const PriceSubTextWhite = styled.div`
  color: white;
  margin-bottom: 1em;
  font-size: 0.8em;
`;
