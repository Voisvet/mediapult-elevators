import * as React from 'react';
import TableRow from './TableRow';
import { Table, TableHead, Th, ThFirst } from './ApplicationTable.styles';
import { Item } from 'elevators/Order/Content/ApplicationsContainer/ApplicationsList';

interface AtProps {
    items: Item[];
}

interface AtState {

}

class ApplicationsTable extends React.Component<AtProps, AtState> {

    render() {
        return (
            <div>
                <Table>
                    <TableHead>
                        <tr>
                            <ThFirst>Район</ThFirst>
                            <Th>Лифтов</Th>
                            <Th>Аудитория</Th>
                            <Th>Формат</Th>
                            <Th>Компания</Th>
                            <Th>Стоимость</Th>
                            <Th/>
                        </tr>
                    </TableHead>
                    <tbody>
                    {this.props.items.map((item, id) => (<TableRow
                        name={item.name}
                        address={item.address}
                        count={item.count}
                        price={item.price}
                        discount={item.discount}
                        audience={item.audience}
                        format={item.format}
                        company={item.company}
                        inCart={item.inCart}
                    />))}

                    {/*<TableRow*/}
                    {/*    name={'Эгершельд, Лифт №2'}*/}
                    {/*    address={'ул. Адмирала Юмашева, 35'}*/}
                    {/*    count={25}*/}
                    {/*    price={25000}*/}
                    {/*    discount={2500}*/}
                    {/*    audience={345025}*/}
                    {/*    format={'A4'}*/}
                    {/*    company={'Владлифт'}*/}
                    {/*    inCart={false}*/}
                    {/*/>*/}
                    {/*<TableRow*/}
                    {/*    name={'Эгершельд, Лифт №2'}*/}
                    {/*    address={'ул. Адмирала Юмашева, 35'}*/}
                    {/*    count={25}*/}
                    {/*    price={25000}*/}
                    {/*    discount={2500}*/}
                    {/*    audience={345025}*/}
                    {/*    format={'A4'}*/}
                    {/*    company={'Владлифт'}*/}
                    {/*    inCart={true}*/}
                    {/*/>*/}
                    </tbody>
                </Table>
            </div>
        );
    }
}

export default ApplicationsTable;
