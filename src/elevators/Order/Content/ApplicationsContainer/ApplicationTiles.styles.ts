import * as styled from 'styled-components';

export const TilesContainer = styled.div`
    display: flex;
    justify-content: center;
    flex-wrap: wrap;
`;
