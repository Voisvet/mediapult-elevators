import * as React from 'react';
import ApplicationsContainer from './ApplicationsContainer/ApplicationsContainer';
import Breadcrumbs from './Breadcrumbs/Breadcrumbs';
import FiltersContainer from './FiltersContainer/FiltersContainer';

import { ContentContainer, OrderFormContainer } from './Content.styles';

class Content extends React.Component<any, any> {
    componentWillMount(): void {
        this.clearFilters();
    }

    getDistricts = () => {
        const result = [];
        result.push({
            value: 'Все районы',
            label: 'Все районы'
        });
        this.props.offers.forEach(item => {
            if (!result.includes(item.address)) {
                result.push({
                    value: item.address,
                    label: item.address
                });
            }
        });
        return result;
    };

    getOffers = () => {
        let result;

        if (this.state.filters.district.value !== 'Все районы') {
            result = this.props.offers.filter(item => item.address === this.state.filters.district.value);
        } else {
            result = this.props.offers;
        }

        result = result.filter(item => item.audience >= this.state.filters.audience[0]
                                    && item.audience <= this.state.filters.audience[1]);

        result = result.filter(item => item.price >= this.state.filters.price[0]
            && item.price <= this.state.filters.price[1]);

        return result;
    };

    changeFilter = (filterName, value) => {
        switch (filterName) {
            case 'district':
                this.setState({filters: {...this.state.filters, district: value}});
                break;
            case 'price':
                this.setState({filters: {...this.state.filters, price: value}});
                break;
            case 'audience':
                this.setState({filters: {...this.state.filters, audience: value}});
                break;
            default:
                break;
        }
    };

    clearFilters = () => {
        const audience = this.props.offers.map(val => val.audience);
        const price = this.props.offers.map(val => val.price);

        this.setState({
            filters: {
                audience: [Math.min(...audience), Math.max(...audience)],
                price: [Math.min(...price), Math.max(...price)],
                district: { label: 'Все районы', value: 'Все районы' }
            }
        });

    };

    render() {
        const audience = this.props.offers.map(val => val.audience);
        const price = this.props.offers.map(val => val.price);
        return (
            <ContentContainer>
                <Breadcrumbs/>
                <OrderFormContainer>
                    <FiltersContainer
                        districts={this.getDistricts()}
                        audience={{
                            min: Math.min(...audience),
                            max: Math.max(...audience),
                            value: this.state.filters.audience
                        }}
                        price={{
                            min: Math.min(...price),
                            max: Math.max(...price),
                            value: this.state.filters.price
                        }}
                        onFiltersChange={this.changeFilter}
                        formats={Array.from(new Set(this.props.offers.map(val => val.format)))}
                        filters={this.state.filters}
                        clearFilters={this.clearFilters}
                    />
                    <ApplicationsContainer
                        items={this.getOffers()}
                    />
                </OrderFormContainer>
            </ContentContainer>
        );
    }
}

export default Content;
