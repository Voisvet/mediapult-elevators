import * as styled from 'styled-components';

export const HeaderContainer = styled.div`
    flex: 0 0 auto;
    background: linear-gradient(to top, #1890ea, #00adef);
    padding: 16px 24px;
    display: flex;
    justify-content: space-between;
    align-items: center;
`;

export const Button = styled.button`
    background: none;
    border: none;
    color: #ffffff;
    font-weight: 700;
    padding: 10px 20px;
    cursor: pointer;
    font-size: 18px;
    
    &:hover, &:active {
        color: rgba(255, 255, 255, 0.7);
    }
`;

export const ActiveButton = styled(Button)`
    background: #1465a6;
    border-radius: 4px;
    
    &:hover, &:active {
        color: #ffffff;
    }
`;

export const Logo = styled.div`
    display: flex;
    flex-direction: column;
    color: #ffffff;
`;

export const LogoBigText = styled.h1`
    text-transform: uppercase;
    font-weight: 500;
    font-size: 36px;
`;

export const LogoSmallText = styled.h2`
    font-size: 12px;
`;
