import * as React from 'react';

import {
  ActiveButton,
  Button,
  HeaderContainer,
  Logo,
  LogoBigText,
  LogoSmallText
} from 'elevators/Order/Header/Header.styles';
// import Logo from '../../Landing/Logo/Logo';

class Header extends React.Component {
  render() {
    return (
      <HeaderContainer>
        <Logo>
          <LogoBigText>Media<span style={{color: '#354052'}}>Pult</span></LogoBigText>
          <LogoSmallText>Агрегатор оффлайн рекламы</LogoSmallText>
        </Logo>
        <div>
          <Button>Реклама на радио</Button>
          <Button>Реклама на транспорте</Button>
          <ActiveButton class="active">Реклама в лифтах</ActiveButton>
        </div>
        <div className="personal"> </div>
      </HeaderContainer>
    );
  }
}

export default Header;
