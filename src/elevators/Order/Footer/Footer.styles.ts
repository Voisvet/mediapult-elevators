import * as styled from 'styled-components';

export const FooterContainer = styled.div`
    flex: 0 0 50px;
    background: linear-gradient(to top, #1890ea, #00adef);
`;
