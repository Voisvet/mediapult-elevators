import * as React from 'react';

import { FooterContainer } from 'elevators/Order/Footer/Footer.styles';

class Footer extends React.Component {
  render() {
    return (
      <FooterContainer>
        It's footer
      </FooterContainer>
    );
  }
}

export default Footer;
