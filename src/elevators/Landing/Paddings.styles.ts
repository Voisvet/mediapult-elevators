import * as styled from 'styled-components';

export const mainPadding = styled.css`
    padding:7rem;
    
	/* Portrait tablets and medium desktops */
	@media (min-width: 992px) and (max-width: 1199px) {
	padding:7rem;
	}
	/* Portrait tablets and small desktops */
	@media (min-width: 768px) and (max-width: 991px) {
	padding:5rem;
	}
	
	@media (min-width: 481px) and (max-width: 767px) {
	padding:7rem;
	}
	
	/* Landscape phones and smaller */
	@media (max-width: 480px) {
	    padding: 10vmin;
	}
`;

export const leftRightPadding = styled.css`
    padding-left:5rem;
	padding-right:5rem;
	
	@media (min-width: 992px) and (max-width: 1199px) {
	padding-left:4rem;
	padding-right:4rem;
	}
	
	@media (min-width: 768px) and (max-width: 991px) {
	padding-left:3rem;
	padding-right:3rem;
	}
	
	@media (min-width: 481px) and (max-width: 767px) {
	padding-left:2rem;
	padding-right:2rem;
	}
	
	@media (max-width: 480px) {
	padding-left:2rem;
	padding-right:2rem;
	}
`;

export const topPadding = styled.css`
    padding-top:5rem;
	
	@media (min-width: 992px) and (max-width: 1199px) {
	padding-top:4rem;
	}
	
	@media (min-width: 768px) and (max-width: 991px) {
	padding-top:3rem;
	}
	
	@media (min-width: 481px) and (max-width: 767px) {
	padding-top:2rem;
	}
	
	@media (max-width: 480px) {
	padding-top:2rem;
	}
`;

export const bottomPadding = styled.css`
    padding-bottom:5rem;
	
	@media (min-width: 992px) and (max-width: 1199px) {
	padding-bottom:4rem;
	}
	
	@media (min-width: 768px) and (max-width: 991px) {
	padding-bottom:3rem;
	}
	
	@media (min-width: 481px) and (max-width: 767px) {
	padding-bottom:2rem;
	}
	
	@media (max-width: 480px) {
	padding-bottom:2rem;
	}
`;

export const smallTopPadding = styled.css`
    padding-top:2.5rem;
	
	@media (min-width: 992px) and (max-width: 1199px) {
	padding-top:2rem;
	}
	
	@media (min-width: 768px) and (max-width: 991px) {
	padding-top:1.5rem;
	}
	
	@media (min-width: 481px) and (max-width: 767px) {
	padding-top:1rem;
	}
	
	@media (max-width: 480px) {
	padding-top:1rem;
	}
`;

export const smallBottomPadding = styled.css`
    padding-bottom:2.5rem;
	
	@media (min-width: 992px) and (max-width: 1199px) {
	padding-bottom:2rem;
	}
	
	@media (min-width: 768px) and (max-width: 991px) {
	padding-bottom:1.5rem;
	}
	
	@media (min-width: 481px) and (max-width: 767px) {
	padding-bottom:1rem;
	}
	
	@media (max-width: 480px) {
	padding-bottom:1rem;
	}
`;
