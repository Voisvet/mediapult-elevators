import * as styled from 'styled-components';
import ArgumentsPart from './ArgumentsPart/ArgumentsPart';
import { ButtonStyled, H2 } from '../Landing.styles';
import { slideInDown } from '../Animation.styles';
import { darkBlueColor } from '../Colors.styles';
import { mainPadding, leftRightPadding, topPadding, bottomPadding } from "../Paddings.styles";
import { MaxWidth } from "../Landing.styles";

export const Header = styled(H2)`
	color:${darkBlueColor};
`;
export const ArgumentsBlock = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    background-color: white;
    color: ${darkBlueColor};
    width: 100%;
    ${leftRightPadding};
    ${MaxWidth};
    ${bottomPadding};
    ${topPadding};
`;

const animation = styled.css`
	animation: ${slideInDown} 1200ms 1;
`;

export const AnimatedArguments = styled.div`
	${props => (props.isInViewState ? animation : '')}
`;
