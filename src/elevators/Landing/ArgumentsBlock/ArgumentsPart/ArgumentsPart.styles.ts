import * as styled from 'styled-components';
import { H1, H2, H3 } from '../../Landing.styles';
import { darkBlueColor, lightBlueColor } from '../../Colors.styles';

const ArgumentItem = styled.div`
	display:grid;
 	align-items: center;
    justify-content: center;
    
    @media (max-width: 767px) and (orientation: portrait) and (min-width: 481px){
    grid-column:1;
    }
    @media (max-width: 480px){
    grid-column:1;
    }
    `;
export const ArgumentItem1 = styled(ArgumentItem)`
grid-column:1/3;
@media (min-width: 768px) and (max-width: 991px) {
   grid-column:1/3;
    }
    @media (orientation: landscape)and (max-width: 991px) and (min-width:481px){
   grid-column:1/3;
    } 
`;
export const ArgumentItem2 = styled(ArgumentItem)`
grid-column:3/5;
@media (min-width: 768px) and (max-width: 991px) {
   grid-column:3/5;
    }
    @media (orientation: landscape)and (max-width: 991px) and (min-width:481px){
   grid-column:3/5;
    } 
`;
export const ArgumentItem3 = styled(ArgumentItem1)`
grid-column:5/7;
`;
export const ArgumentItem4 = styled(ArgumentItem2)`
grid-column:2/4;
`;
export const ArgumentItem5 = styled(ArgumentItem)`
grid-column:4/6;
@media (min-width: 768px) and (max-width: 991px) {
   grid-column:2/4;
    }
    @media (orientation: landscape)and (max-width: 991px) and (min-width:481px){
   grid-column:2/4;
    } 
`;
export const ImgRow = styled.div`
    display:grid;
    grid-template-columns: repeat(3, 1fr);
    grid-auto-rows:minmax(100px, auto);
    justify-items:center;
    align-items:center;    
`;
export const Icon = styled.img`
	display:grid;
    src: url(${props => props.src});
    alt: ${props => props.alt};
    grid-column:2/3;
    width:100%;
`;
export const Row = styled.div`
    margin:2em 0;
    display:grid;
    grid-template-columns: repeat(6, 1fr);
    grid-gap:1.5em;
    grid-auto-rows:minmax(100px, auto);
    justify-items:center;
    align-items:start;
    
    @media (min-width: 768px) and (max-width: 991px) {
    grid-template-columns: repeat(4, 1fr);
    }
    @media (orientation: landscape)and (max-width: 991px) and (min-width:481px){
    grid-template-columns: repeat(4, 1fr);
    }
    @media (max-width: 767px) and (orientation: portrait) and (min-width: 481px){
    grid-template-columns: 1fr;
    grid-gap:2em 5em;
    }
    @media (max-width: 480px){
    grid-template-columns: 1fr;
    grid-gap:2em 5em;
    }
`;
export const TextContainer = styled (H1)`
max-width:80%;
`;
export const Header = styled(H2)`
	color:${lightBlueColor};
	margin-top:0.5rem;
 `;

export const Text = styled (H3)`
	color:${darkBlueColor};
	margin-top:0.5rem;
`;
