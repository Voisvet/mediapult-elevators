import * as React from 'react';

import { Row } from './ArgumentsPart.styles';
import { ImgRow, ArgumentItem1, ArgumentItem2, ArgumentItem3, ArgumentItem4, ArgumentItem5,
    Icon, Header, Text, TextContainer } from './ArgumentsPart.styles';
import { textForArgumentsPart as text, headerForArgumentsPart as header } from '../../Strings.styles';

const search = require('../../../../assets/Icons/1search.svg') as string;
const folders = require('../../../../assets/Icons/2folders.svg') as string;
const billboard = require('../../../../assets/Icons/3billboard.svg') as string;
const stationary = require('../../../../assets/Icons/5stationery.svg') as string;
const doc = require('../../../../assets/Icons/4doc.svg') as string;

class ArgumentsPart extends React.Component<any, any> {
    render(): React.ReactNode {
        return (
            <Row>
                <ArgumentItem1>
                    <ImgRow><Icon alt="icon1" src={search}/></ImgRow>
                    <TextContainer><Header>{header.h1}</Header><Text>{text.t2}</Text></TextContainer>
                </ArgumentItem1>
                <ArgumentItem2>
                    <ImgRow> <Icon alt="icon2" src={folders}/></ImgRow>
                    <TextContainer><Header>{header.h2}</Header><Text>{text.t2}</Text></TextContainer>
                </ArgumentItem2>
                <ArgumentItem3>
                    <ImgRow><Icon alt="icon3" src={billboard}/></ImgRow>
                    <TextContainer><Header>{header.h3}</Header><Text>{text.t3}</Text></TextContainer>
                </ArgumentItem3>
                <ArgumentItem4>
                    <ImgRow><Icon alt="icon4" src={doc}/></ImgRow>
                    <TextContainer><Header>{header.h4}</Header><Text>{text.t4}</Text></TextContainer>
                </ArgumentItem4>
                <ArgumentItem5>
                    <ImgRow><Icon alt="icon5" src={stationary}/></ImgRow>
                    <TextContainer><Header>{header.h5}</Header><Text>{text.t5}</Text></TextContainer>
                </ArgumentItem5>
            </Row>
        );
    }
}

export default ArgumentsPart;
