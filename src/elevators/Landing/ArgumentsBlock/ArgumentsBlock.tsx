import * as React from 'react';

import { AnimatedArguments, Header, ArgumentsBlock as StyledArgumentsBlock } from './ArgumentsBlock.styles';

import ArgumentsPart from './ArgumentsPart/ArgumentsPart';
import { Waypoint } from 'react-waypoint';
import { H1 as TextContainer } from '../Landing.styles';

class ArgumentsBlock extends React.Component<any, any> {
    constructor(props) {
        super(props);
        this.state = {
            isInView: false
        };
        this.onEnter = this.onEnter.bind(this);

    }
    onEnter({ previousPosition }){
        if (previousPosition === Waypoint.below){
            this.setState({
                isInView: true
            });
        } else {
			this.setState({
				isInView: false
			});
		}
    }
    render(): React.ReactNode {
        return (
            <StyledArgumentsBlock>
                <TextContainer><Header>Почему реклама в лифтах?</Header></TextContainer>
				<Waypoint onEnter={this.onEnter}/>
                <AnimatedArguments isInViewState={this.state.isInView}><ArgumentsPart/></AnimatedArguments>
            </StyledArgumentsBlock>
        );
    }
}

export default ArgumentsBlock;
