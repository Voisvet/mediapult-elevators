import * as styled from 'styled-components';
import { darkBlueColor, whiteColor } from '../Colors.styles';
import {TextContainer, H4} from "../Landing.styles";

export const Logo = styled.div`
	display:grid;
	grid-template-columns:1fr;
	justify-self: start;
`;

export const Header = styled(TextContainer)`
	font-weight:500;
	text-align:center;
	display:inline;
`;
export const Header1 = styled.div`
   display:inline;
   justify-self:center;
	@media (max-width: 767px) {
		display:none;	
	}
`;
export const Header2 = styled.div`
	display:none;
	justify-self:center;
	@media (max-width: 767px) {
		display:block;	
	}
`;
export const White = styled(Header)`
	color:${whiteColor};
	text-align:left;
	@media (max-width: 767px) {
		display:inline;
	}
`;

export const Dark = styled(Header)`
	color:${darkBlueColor};
	text-align:right;
	@media (max-width: 767px) {
		display:inline;
	}
`;

export const Text = styled(H4)`
	color:white;
	grid-column:1/3;
	justify-self:center;
	text-align:center;
	
	/* Landscape phones and portrait tablets */
	@media (max-width: 767px) {
		display:none;
	}
`;
