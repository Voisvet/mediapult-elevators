import * as React from 'react';
import { TextContainer } from '../Landing.styles';
import { Logo, Text, Header1, Header2, White, Dark } from './Logo.styles';

class LogoBlock extends React.Component<any, any> {
    render(): React.ReactNode {
        return (
            <Logo>
               <Header1>
                   <White>MEDIA </White><Dark>PULT</Dark>
               </Header1>
                <Header2>
                    <White>M </White><Dark>P</Dark>
                </Header2>
                <TextContainer><Text>Агреготор онлайн рекламы</Text></TextContainer>
            </Logo>
        );
    }
}

export default LogoBlock;
