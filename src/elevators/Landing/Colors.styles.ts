export const lightBlueColor = '#00aeef';
export const darkBlueColor = '#354052';
export const whiteColor = '#fff';
export const hoveredLightBlueColor = '#afd8f8';
export const grayBackground = '#eff3f6';

export const darkOrangeBackground = '#ef600c';
export const lightOrangeBackground = '#f68d0c';
export const grayBorderColor = '#eaeaea';
