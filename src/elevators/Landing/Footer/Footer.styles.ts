import * as styled from 'styled-components';
import {MaxWidth} from "../Landing.styles";

export const FooterBlock = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    width: 100%;
    ${MaxWidth};
`;
