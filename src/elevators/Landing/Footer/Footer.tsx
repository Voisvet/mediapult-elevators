import * as React from 'react';

import { FooterBlock as StyledFooter } from './Footer.styles';

import InformationBlock  from './InformationBlock/InformationBlock';
import LastChanceBlock from './LastChanceBlock/LastChanceBlock';
import ContactsBlock from './ContactsBlock/ContactsBlock';

class FooterBlock extends React.Component<any, any> {
    render(): React.ReactNode {
        return (
            <StyledFooter>
                <InformationBlock/>
                <LastChanceBlock/>
                <ContactsBlock/>
            </StyledFooter>
        );
    }
}

export default FooterBlock;
