import * as styled from 'styled-components';
import { ButtonStyled } from '../../Landing.styles';
import { H2 } from '../../Landing.styles';
import { darkBlueColor, grayBackground, whiteColor, hoveredLightBlueColor } from '../../Colors.styles';
import { heartBeat } from '../../Animation.styles';
import { mainPadding, leftRightPadding, topPadding, bottomPadding }  from '../../Paddings.styles';

export const Header = styled(H2)`
color:${darkBlueColor};
width:100%;
`;

export const LastChance = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    flex: 1 0 auto;
    background-color:${grayBackground};
    width:100%;
    ${topPadding};
    ${leftRightPadding};
    ${bottomPadding};
`;

const buttonHeartBeat = styled.css`
	animation: ${heartBeat} 2000ms infinite;
`;

export const AnimatedButton = styled(ButtonStyled)`
	${props => (props.isInViewState ? buttonHeartBeat : '')}
`;
