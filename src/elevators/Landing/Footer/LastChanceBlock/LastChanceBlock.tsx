import * as React from 'react';

import { AnimatedButton, Header, LastChance } from './LastChanceBlock.styles';
import { Waypoint } from 'react-waypoint';
import {headerForLastChance as header} from "../../Strings.styles";
import {H1 as TextContainer, H3} from "../../Landing.styles";

class LastChanceBlock extends React.Component<any, any> {
    constructor(props) {
        super(props);
        this.state = {
            isInView: false
        };
        this.onEnter = this.onEnter.bind(this);

    }
    onEnter ({ previousPosition }) {
        if (previousPosition === Waypoint.below) {
            this.setState({
                isInView: true
            });
        } else {
            this.setState({
                isInView: false
            });
        }
    }
    render(): React.ReactNode {

        return (
            <LastChance>
                <Waypoint onEnter={this.onEnter}/>
                <TextContainer>
                    <Header>{header.h1}</Header>
                    <AnimatedButton isInViewState={this.state.isInView}>
                        <TextContainer><H3>Попробуйте сейчас</H3></TextContainer>
                    </AnimatedButton>
                </TextContainer>
            </LastChance>
        );
    }
}

export default LastChanceBlock;
