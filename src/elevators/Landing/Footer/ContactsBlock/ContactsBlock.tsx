import * as React from 'react';
import { Links, Icon, NavBar, Navigation, Phone } from './ContacksBlock.styles';
import Logo from '../../Logo/Logo';
import {H4, H2, TextContainer } from "../../Landing.styles";

class ContactsBlock extends React.Component<any, any> {
    render(): React.ReactNode {
        // @ts-ignore
        return (
            <NavBar>
                <Logo/>
                <Navigation>
                    <ul>
                        <li><a href="#"><TextContainer><H4>Носители</H4></TextContainer></a></li>
                        <li><a href="#"><TextContainer><H4>Сервис</H4></TextContainer></a></li>
                        <li><a href="#"><TextContainer><H4>Контакты</H4></TextContainer></a></li>
                    </ul>
                </Navigation>
                <Phone><H2>+78008886677</H2></Phone>
                <Links>
                    <TextContainer>
                        <H4>
                            <Icon alt="vk" src={require('../../../../assets/Icons/vk.svg')}/>
                        </H4>
					</TextContainer>
					<TextContainer>
                        <H4>
                            <Icon alt="twitter" src={require('../../../../assets/Icons/twitter.svg')}/>
                        </H4>
					</TextContainer>
					<TextContainer>
                        <H4>
                            <Icon alt="youtube" src={require('../../../../assets/Icons/youtube.svg')}/>
                        </H4>
					</TextContainer>
					<TextContainer>
                        <H4>
                            <Icon alt="facebook" src={require('../../../../assets/Icons/facebook.svg')}/>
                        </H4>
					</TextContainer>
					<TextContainer>
                        <H4>
                            <Icon alt="google" src={require('../../../../assets/Icons/google.svg')}/>
                        </H4>
					</TextContainer>
					<TextContainer>
						<H4>
                        <Icon alt="instagram" src={require('../../../../assets/Icons/instagram.svg')}/>
						</H4>
					</TextContainer>
					<TextContainer>
						<H4>
                        <Icon alt="linkedIn" src={require('../../../../assets/Icons/linkedin.svg')}/>
						</H4>
					</TextContainer>
					<TextContainer>
						<H4>
                        <Icon alt="ok" src={require('../../../../assets/Icons/ok.svg')}/>
						</H4>
                    </TextContainer>
                </Links>
            </NavBar>
        );
    }
}

export default ContactsBlock;
