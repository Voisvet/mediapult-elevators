import * as styled from 'styled-components';
import { TextContainer } from '../../Landing.styles';
import { whiteColor } from '../../Colors.styles';
import { smallTopPadding, leftRightPadding, smallBottomPadding } from "../../Paddings.styles";

export const Phone = styled (TextContainer)`
	grid-column:3;
	justify-self:end;
`;
export const NavBar = styled.div`
    width: 100%;
    padding: 0 1vw;
    align-items:center;
    padding: 2vw 4vw;
    background-image: linear-gradient(to right, #00aeef , #1991eb);
    color: ${whiteColor};
    ${smallTopPadding};
    ${leftRightPadding};
    ${smallBottomPadding};
    display:grid;
    grid-template-columns: 1fr 1fr 1fr;
    grid-gap:1em;
    `;

export const Navigation = styled.nav`
    display: flex;
    justify-content:center;
    align-items:center
    flex: 1;
    
    ul {
        list-style: none;
    }
    
    li {
            display: inline-block;
        	margin: 0 1em;
        @media (max-width: 991px) {
    		 margin: 0 0.5em;
    	}
        
        a:link, a:visited {
            text-transform: capitalize;
            text-decoration: none;
            font-weight: 600;
            color: ${whiteColor};
            transition: color 0.2s;
        }
        
        a:hover, a:active {
            color: #afd8f8;
        }
    }
    @media (max-width:767px){
    	display:none;
    }
`;

export const Icon = styled.img`
	display:grid;
    src: url(${props => props.src});
    alt: ${props => props.alt};
    margin:2vmin 4vmin;
    /* Portrait tablets and medium desktops */
	@media (min-width: 992px) and (max-width: 1399px) {
    	margin:1.5vmin 3vmin;
	}
	
	/* Portrait tablets and small desktops */
	@media (min-width: 768px) and (max-width: 991px) {
    	margin:1vmin 2vmin;
	}
	
	/* Landscape phones and portrait tablets */
	@media (max-width: 767px) {
		margin: 2vmin;
	}
    
`;

export const Links = styled.div`
	grid-column:1/4;
    display:flex;
    justify-content: center;
    align-items:start;
`;
