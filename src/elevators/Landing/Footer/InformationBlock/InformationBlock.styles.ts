import * as styled from 'styled-components';
import { H2, H3 } from '../../Landing.styles';
import { darkBlueColor, lightBlueColor, whiteColor, hoveredLightBlueColor } from '../../Colors.styles';
import { mainPadding, leftRightPadding, topPadding, bottomPadding }  from '../../Paddings.styles';
export const Item = styled.div`
	display:flex;
	flex-direction:column;
	justify-content:center;
	margin:1em;
	@media (max-width: 768px) {
	align-items:center;
	margin:0.5em;
	}
    `;

export const Icon = styled.img`
	display: flex;
	width:12rem;
    src: url(${props => props.src});
    alt: ${props => props.alt};
    @media (min-width: 481px) and (max-width: 767px) {
	width:10rem;
	}   
   @media (max-width: 480px) {
	width: 8rem;
	}
    
`;
export const Row = styled.div`
    display:flex;
    flex-direction:row;
    justify-content:center;  
    align-content:center;
    ${leftRightPadding};
    ${topPadding};
    ${bottomPadding};
    width:60%;
    /* Portrait tablets and medium desktops */
	@media (min-width: 992px) and (max-width: 1199px) {
	width:70%;
	}
	/* Portrait tablets and small desktops */
	@media (min-width: 768px) and (max-width: 991px) {
	width:80%;
	}
    @media (max-width: 767px) {
    flex-direction:column;
    width:100%;
    }
`;

export const Header = styled(H2)`
	width:100%;
	color:${lightBlueColor};
	margin-left:0!important;
	margin-right:0!important;
	text-align:left!important;
	@media (max-width: 767px) {
    text-align:center!important;
    margin-left: auto!important;
    margin-right: auto!important;
    }
 `;

export const Text = styled (H3)`	
	color:${darkBlueColor};
	width:100%;
	text-align:left;
	margin-top:1em;
	@media (max-width: 767px) {
    text-align:center;
    margin-left: auto!important;
    margin-right: auto!important;
    }
`;
