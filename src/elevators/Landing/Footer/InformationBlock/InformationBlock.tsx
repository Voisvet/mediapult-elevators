import * as React from 'react';

import { Item, Row, Header, Text, Icon } from './InformationBlock.styles';
import { textForInformationBlock } from "../../Strings.styles";
import { H1 as TextContainer } from "../../Landing.styles";

const lock = require(`./../../../../../src/assets/Icons/lock_opt.png`) as string;
class InformationBlock extends React.Component<any, any> {
    render(): React.ReactNode {
        return (
            <Row>
                <Item>
                    <Icon alt="lock" src={lock}/>
                </Item>
                <Item>
                    <TextContainer> <Header>Стоимость фиксирована</Header>
                    <Text>{textForInformationBlock.t1}
                    </Text>
                   </TextContainer>
                </Item>
            </Row>
        );
    }
}

export default InformationBlock;
