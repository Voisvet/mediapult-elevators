import * as React from 'react';

import { WelcomeBlock as StyledWelcomeBlock } from './WelcomeBlock.styles';

import NavigationBar from './NavigationBar/NavigationBar';
import PayloadPart from './PayloadPart/PayloadPart';
const background = require('../../../assets/Icons/welcome_background_opt.jpg') as string;

class WelcomeBlock extends React.Component<any, any> {
  render(): React.ReactNode {
      return (
            <StyledWelcomeBlock img={background}>
                <NavigationBar/>
                <PayloadPart {...this.props}/>
            </StyledWelcomeBlock>
      );
  }
}

export default WelcomeBlock;
