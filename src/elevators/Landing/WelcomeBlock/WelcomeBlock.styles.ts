import * as styled from 'styled-components';
import { darkBlueColor, lightBlueColor, whiteColor  } from '../Colors.styles';
import {MaxWidth} from "../Landing.styles";
import {bottomPadding, leftRightPadding, smallTopPadding} from "../Paddings.styles";

export const WelcomeBlock = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    background-image:  url(${props => props.img});
    background-position:50% 50%;
    background-size: cover;
    color: ${whiteColor};
    width: 100%;
    background-color:${lightBlueColor};
    ${MaxWidth};
    ${leftRightPadding};
    ${smallTopPadding};
    ${bottomPadding};
    min-height:60vh;
    
    @media (max-width:1380px){
    min-height:100vh;
    @media screen and (orientation:portrait){
    min-height:60vh;
    }
    @media screen and (orientation: landscape){
    min-height:100vh;
    }
    }
`;
