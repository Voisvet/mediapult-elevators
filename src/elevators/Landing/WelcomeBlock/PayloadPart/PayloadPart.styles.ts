import * as styled from 'styled-components';
import { H1 } from '../../Landing.styles';
import { jackInTheBox } from '../../Animation.styles';

export const Header = styled(H1)`
    width:70%;
    /* Portrait tablets and medium desktops */
    @media (min-width: 992px) and (max-width: 1199px) {
    width:80%;
    }
    /* Portrait tablets and small desktops */
    @media (min-width: 768px) and (max-width: 991px) {
    width:90%;
    }
    
    /* Landscape phones and portrait tablets */
    @media (max-width: 767px) {
    width:100%;
    }
`;

export const AnimatedHeader = styled.div`
animation: ${jackInTheBox} 1000ms 1;
`;

export const PayloadPart = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    flex: 1 0 auto;
`;
