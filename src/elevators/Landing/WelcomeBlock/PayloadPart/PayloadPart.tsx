import * as React from 'react';

import { AnimatedHeader, Header, PayloadPart as StyledPayloadPart } from './PayloadPart.styles';
import { ButtonStyled, H1 as TextContainer, H3 } from '../../Landing.styles';
class PayloadPart extends React.Component<any, any> {
    render(): React.ReactNode {
        return (
            <StyledPayloadPart>
                <Header>
                    <AnimatedHeader>
                        Mediapult позволит вам разместить рекламу в лифтах вашего города в
                        режиме онлайн
                    </AnimatedHeader>
                    <ButtonStyled onClick={() => this.props.sendStateEvent('createOrder')}>
                        <TextContainer><H3>Попробуйте сейчас</H3></TextContainer>
                    </ButtonStyled>
                </Header>
            </StyledPayloadPart>
        );
    }
}

export default PayloadPart;
