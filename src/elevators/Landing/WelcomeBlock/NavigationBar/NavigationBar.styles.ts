import * as styled from 'styled-components';
import { grayBorderColor, lightBlueColor, whiteColor, hoveredLightBlueColor } from '../../Colors.styles';
import { fadeIn } from '../../Animation.styles';

export const NavBar = styled.div`
    width: 100%;
    display:flex;
    justify-content:space-between;
`;

export const Dim = styled.div`
   z-index:1;
   animation: ${fadeIn} 200ms linear;
   background:#000;
    opacity:0.7;
    position:fixed; /* important to use fixed, not absolute */
    top:0;
    left:0;
    width:100%;
    height:100%;
`;

export const Navigation = styled.nav`
   left: 50%!important;
    right: 50%!important;
    position: absolute;
    width: 100%;
    transform: translate(-50%, 0);
    display: flex;
    justify-content:center;
    align-items:center;
    @media (max-width: 767px) { 
    align-items: flex-start;
    }
    ul { list-style: none;
    	@media (max-width: 767px) {    
    		display: flex;
    		flex-direction: column;
    		align-items: flex-start;
    	}
   	}
    
    li {
       display: inline-block;
        margin: 0 2vmin;
        @media (min-width:768px) and (max-width: 991px) {margin: 0 1.5vmin;}
    @media (max-width:767px){ display: contents;}       
        a:link, a:visited {
        	margin: 0 1vmin;
            text-transform: capitalize;
            text-decoration: none;
            font-weight: 600;
            color: inherit;
            transition: color 0.2s;
            @media (max-width: 767px) { 
                padding: 4vmin 4vmin;
   				 margin: 0px 2vmin;
    		}
        }
        a:hover, a:active {color: ${hoveredLightBlueColor};}
    }
    flex-direction:row;
    @media (max-width:767px){
       flex-direction:column;
       position:relative;
       border-bottom:1px solid ${grayBorderColor};           
    }
`;

export const AccountButtons = styled.div`
    display: flex;
    justify-content:flex-end;
    flex-direction:row;
    align-items:flex-end;
    @media (max-width:767px){
       flex-direction:column;
       align-items: flex-start;
       margin-right:auto;       
    }
`;

export const Button = styled.a`
   color:inherit;
    display: inline-block;
    text-transform: capitalize;
    text-decoration: none;
    font-weight: 600;
    background: transparent;
    padding: 1vw 2vw;
    border: none;
    cursor: pointer;
    transition: color 0.2s, border 0.2s;
    ${props => props.outlined && styled.css`

          border: 1px solid ${whiteColor};
          border-radius: 5px;
           @media (max-width:767px){
           border: 0px solid ${whiteColor};
           }
    `};
    
    &:link, &:visited {
        color: inherit;
    }
    
    &:hover, &:active {
        color: ${hoveredLightBlueColor};
        ${props => props.outlined && styled.css`
              border: 1px solid ${hoveredLightBlueColor};
              border-radius: 5px;
                @media (max-width:767px){
                 border: 0px solid ${hoveredLightBlueColor};
                }
        `};
    }
    @media (max-width:767px){
          padding:4vmin 4vmin;
          margin: 0 2vmin;
    }
`;

export const Wrapper = styled.div`
   width: max-content;
   display: flex;
   align-items: center;
   flex-direction:row;
   justify-content:space-between;
   @media (max-width:767px){
   align-items: flex-end;
      flex-direction:column;
   }
`;

// content that should be hidden in mobile view
export const Sidebar = styled.div`
   z-index:1;
    align-items: center;
   display:flex;
   justify-content:flex-end;
   width:100%;
   @media (min-width:768px){
      display: flex!important;
   }
   @media (max-width:767px){
      flex-direction:column;
      position:absolute;
      background-color:white;
      color:rgb(53, 64, 82);
      border-radius: 4px;
       box-shadow: rgba(0,0,0,0.1) 0 0 8px;
      opacity:${props => props.isOpen === true ? 1 : 0};
      visibility: ${props => props.isOpen === true ? 'visible' : 'hidden'};
      transition: opacity 400ms ease-out;
      width: min-content;
      left: 50%;
       position: fixed;
       transform: translate(-50%, -50%);
       right: 50%;
       top: 50%;
   }
   
   @media (max-width:767px) and (orientation: landscape){
   max-height:80vh;
   }
   top:50px;
   flex-direction:row;
`;

// hamburger component
export const MenuToggle = styled.button`   
   display:none;
   position: relative;
   width: 26px;
   height: 40px;
  
   @media (max-width: 767px) {
   display:${props => props.fromTop > 5 && props.isOpen === false ? 'none' : 'initial'}; 
    position:fixed;
    z-index: 700;
  }
  border:none;
  background-color: rgba(52, 52, 52, 0);
  
   &:before, &:after{
   background-color: white;
    content: "";
    position: absolute;
  top: 0;
  right: 0;
  display: block;
  width: 26px;
  height: 2px;
  transition: transform 0.3s ease-in;
   }
  &:before {
  transform: ${props => props.isOpen === false ? 'translateY(15px)' : 'translate(0, 20px) rotate(45deg)'};
}
&:after {
  transform: ${props => props.isOpen === false ? 'translateY(23px)' : 'translate(0, 20px) rotate(-45deg)'};
}
`;
