import * as React from 'react';
import {Dim, Wrapper, MenuToggle, Sidebar, AccountButtons, Button, NavBar, Navigation} from './NavigationBar.styles';
import {H4, H1 as TextContainer} from '../../Landing.styles';
import Logo from '../../Logo/Logo';

class NavigationBar extends React.Component<any, any> {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: false,
            fromTop: window.scrollY,
        };
    }

    toggleMenu() {
        this.setState({isOpen: !this.state.isOpen});
    }

    // Adds an event listener when the component is mount.
    componentDidMount() {
        window.addEventListener("scroll", this.handleScroll);
    }

    // Remove the event listener when the component is unmount.
    componentWillUnmount() {
        window.removeEventListener("scroll", this.handleScroll);
    }

    // Hide or show the menu.
    handleScroll = () => {
        this.setState({
            fromTop: window.scrollY,
        });
    };

    render(): React.ReactNode {
        // @ts-ignore
        return (
            <NavBar>
                <Logo/>
                <Wrapper>
                    <MenuToggle onClick={() => this.toggleMenu()} isOpen={this.state.isOpen}
                                fromTop={this.state.fromTop}/>
                    {this.state.isOpen && <Dim onClick={() => this.toggleMenu()}/>}
                    <Sidebar isOpen={this.state.isOpen}>
                        <Navigation fromTop={this.state.fromTop}>
                            <ul>
                                <li><a href="#"><TextContainer><H4>Носители</H4></TextContainer></a></li>
                                <li><a href="#"><TextContainer><H4>Сервис</H4></TextContainer></a></li>
                                <li><a href="#"><TextContainer><H4>Контакты</H4></TextContainer></a></li>
                            </ul>
                        </Navigation>
                        <AccountButtons>
                            <Button href="#"><TextContainer><H4>Войти</H4></TextContainer></Button>
                            <Button href="#" outlined={true}>
                                <TextContainer>
                                    <H4>Регистрация</H4>
                                </TextContainer>
                            </Button>
                        </AccountButtons>
                    </Sidebar>
                </Wrapper>
            </NavBar>
        );
    }
}

export default NavigationBar;
