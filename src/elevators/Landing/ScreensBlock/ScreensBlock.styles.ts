import * as styled from 'styled-components';

import { H2, H3, MaxWidth } from '../Landing.styles';
import { darkBlueColor, lightBlueColor } from '../Colors.styles';
import { fadeInLeft, fadeInRight } from '../Animation.styles';
import {leftRightPadding,topPadding} from "../Paddings.styles";

export const Header = styled(H2)`
	color:${lightBlueColor};
	width:70%;
	/* Portrait tablets and medium desktops */
	@media (min-width: 992px) and (max-width: 1199px) {
	width:80%;
	}
	/* Portrait tablets and small desktops */
	@media (min-width: 768px) and (max-width: 991px) {
	width:90%;
	}
	
	/* Landscape phones and portrait tablets */
	@media (max-width: 767px) {
	width:100%;
	}
`;
export const Text = styled(H3)`
	width:70%;
	color:${darkBlueColor};
	text-align:center;
	margin-top:1em;
	margin-bottom:1em;
	@media (max-width: 991px) {
    width:80%;
    }	
`;

export const Screens = styled.div`
   display: flex;
    flex-direction: column;
    align-items: center;
    background-color: white;
    width: 100%;
   ${leftRightPadding};
    ${MaxWidth};
    ${topPadding};
`;

export const ImgOverlay = styled.div`
display:flex;
flex-direction:row-reverse;
align-items:flex-end;
justify-content: center;
margin-top:4vw;
    width: 100%;
`;

export const Img2 = styled.img`
	src: url(${props => props.src});
	alt: ${props => props.alt};
	width:50%;
	object-position: right;
	margin-left:-10%;
	box-shadow: 0.2vw -0.2vw 1vw 0.2vw rgba(0,0,0,0.15);
	transition: box-shadow .5s;
	&:hover{
  		box-shadow: 0.2vw -0.2vw 1vw 0.6vw rgba(0,0,0,0.20);
	}
	${props => (props.isInViewState ? imgAnimationRight : '')}
`;

const imgAnimationRight = styled.css`
	animation: ${fadeInRight} 1500ms 1;
`;

const imgAnimationLeft = styled.css`
	animation: ${fadeInLeft} 1500ms 1;
`;

export const Img1 = styled.img`
	src: url(${props => props.src});
	alt: ${props => props.alt};
	width:50%;
	float:left;
	margin-right:-10%;
	box-shadow:	0vw -0.2vw 1vw 0.2vw rgba(0,0,0, 0.15);
	transition: box-shadow .5s;
	&:hover{
  box-shadow: 0vw -0.2vw 1vw 0.6vw rgba(0,0,0, 0.20);
	}
	${props => (props.isInViewState ? imgAnimationLeft : '')}
`;
