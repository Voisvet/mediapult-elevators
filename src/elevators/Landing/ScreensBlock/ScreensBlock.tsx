import * as React from 'react';
import { Waypoint } from 'react-waypoint';
import { Text, Header, Screens, ImgOverlay, Img1, Img2 } from './ScreensBlock.styles';
import { textForScreens as text, headerForScreens as header } from '../Strings.styles';
const screen2 = require('../../../assets/Icons/screen2.jpg') as string;
const screen1 = require('../../../assets/Icons/screen1.jpg') as string;
import { H1 as TextContainer } from "../Landing.styles";

class ScreensBlock extends React.Component<any, any> {
    constructor(props) {
        super(props);
        this.state = {
            isInView: false
        };
        this.onEnter = this.onEnter.bind(this);

    }

        onEnter({ previousPosition }){
        if (previousPosition === Waypoint.below) {
            this.setState({
                isInView: true
            });
        } else {
            this.setState({
                isInView: false
            });
        }
    }

    render(): React.ReactNode {

        return (
            <Screens>
                <TextContainer><Header>{header.h1}</Header><Text>{text.t1}</Text></TextContainer>
                <Waypoint onEnter={this.onEnter}/>
                <ImgOverlay>
                    <Img2 isInViewState={this.state.isInView} alt="screen2" src={screen2}/>
                    <Img1 isInViewState={this.state.isInView} alt="screen1" src={screen1}/>
                </ImgOverlay>
            </Screens>
        );
    }
}

export default ScreensBlock ;
