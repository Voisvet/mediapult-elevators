import * as styled from 'styled-components';

// animation for LastChanceBlock
export const heartBeat = styled.keyframes`
	0% {
		-webkit-transform: scale(1);
		transform: scale(1);
	}

	14% {
		-webkit-transform: scale(1.1);
		transform: scale(1.1);
	}

	28% {
		-webkit-transform: scale(1);
		transform: scale(1);
	}

	42% {
		-webkit-transform: scale(1.1);
		transform: scale(1.1);
	}

	70% {
		-webkit-transform: scale(1);
		transform: scale(1);
	}
`;

// animation for screensBlock
export const fadeInLeft = styled.keyframes`
	from {
		-webkit-transform: translate3d(-13.1vw, 0, 0);
		transform: translate3d(-13.1vw, 0, 0);
	}

	to {
		-webkit-transform: translate3d(0, 0, 0);
		transform: translate3d(0, 0, 0);
	}
`;

export const fadeInRight = styled.keyframes`
	from {
		-webkit-transform: translate3d(13.1vw, 0, 0);
		transform: translate3d(13.1vw, 0, 0);
	}

	to {
		-webkit-transform: translate3d(0, 0, 0);
		transform: translate3d(0, 0, 0);
	}
`;

// animation for payloadBlock
export const jackInTheBox = styled.keyframes`
from {
		opacity: 0;
		-webkit-transform: scale(0.1) rotate(30deg);
		transform: scale(0.1) rotate(30deg);
		-webkit-transform-origin: center bottom;
		transform-origin: center bottom;
	}

	50% {
		-webkit-transform: rotate(-5deg);
		transform: rotate(-5deg);
	}

	70% {
		-webkit-transform: rotate(3deg);
		transform: rotate(3deg);
	}

	to {
		opacity: 1;
		-webkit-transform: scale(1);
		transform: scale(1);
	}
`;

export const slideInDown = styled.keyframes`
		from {
		opacity: 0;
		-webkit-transform: scale3d(0.3, 0.3, 0.3);
		transform: scale3d(0.3, 0.3, 0.3);
	}

	50% {
		opacity: 1;
	}
	`;

export const fadeIn = styled.keyframes`
  from {
    opacity: 0;
  }

  to {
    opacity: 0.7;
  }
`;

export const flipInY = styled.keyframes`
	from {
		-webkit-transform: perspective(400px) rotate3d(0, 1, 0, 90deg);
		transform: perspective(400px) rotate3d(0, 1, 0, 90deg);
		-webkit-animation-timing-function: ease-in;
		animation-timing-function: ease-in;
		opacity: 0;
	}

	40% {
	-webkit-transform: perspective(400px) rotate3d(0, 1, 0, -20deg);
	transform: perspective(400px) rotate3d(0, 1, 0, -20deg);
	-webkit-animation-timing-function: ease-in;
	animation-timing-function: ease-in;
}

	60% {
	-webkit-transform: perspective(400px) rotate3d(0, 1, 0, 10deg);
	transform: perspective(400px) rotate3d(0, 1, 0, 10deg);
	opacity: 1;
}

	80% {
	-webkit-transform: perspective(400px) rotate3d(0, 1, 0, -5deg);
	transform: perspective(400px) rotate3d(0, 1, 0, -5deg);
}

	to {
		-webkit-transform: perspective(400px);
		transform: perspective(400px);
	}
`;
