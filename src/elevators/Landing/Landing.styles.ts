import * as styled from 'styled-components';
import { darkOrangeBackground, lightOrangeBackground } from './Colors.styles';
export const TextContainer = styled.div`
		font-size:1.5rem;
		
	/* Portrait tablets and medium desktops */
	@media (min-width: 992px) and (max-width: 1199px) {
	font-size:1.6rem;
	}
	/* Portrait tablets and small desktops */
	@media (min-width: 768px) and (max-width: 991px) {
	font-size:1.4rem;
	}
	
	/* Landscape phones and portrait tablets */
	@media (min-width: 481px) and (max-width: 767px) {
	font-size:1.2rem;
	}
	/* Landscape phones and smaller */
	@media (max-width: 480px) {
	font-size:1rem;
	
	}
`;
export const H1 = styled(TextContainer)`
    	line-height: 1.2;
		font-weight: 400;
		font-weight: ${props => props.weight};
		max-width: ${props => props.minWidth};
		color:${props => props.color};
		margin: ${props => props.margin};
		text-align: center;
		margin-left:auto;
		margin-right:auto;
`;

export const H2 = styled(H1)`
		font-size:0.888em!important;
`;

export const H3 = styled.div`
	display: flex;
	flex-wrap:wrap;
	margin-left:auto;
	margin-right:auto;
	font-size:0.5em!important;
`;

export const H4 = styled.div`
		font-size:0.44em;
		margin-left:auto;
		margin-right:auto;
		@media (max-width:767px){
		font-size:0.8em;
		}
`;
export const ButtonStyled = styled.button`   
   		display:block;
        background: linear-gradient(to top, #f36a19, #f7951c);
        border: none;
        border-radius: 10rem;
        color: #ffffff;
        padding: 1em 3em;
        margin-top: 1.5em;
        font-weight: 500;
        @media (max-width: 767px) {
    		margin-top: 2em;
    	}
        margin-left:auto;
        margin-right:auto;
        font-weight:600;
        cursor: pointer;
       
    &::hover, &::active {
        background: linear-gradient(to top, ${darkOrangeBackground}, ${lightOrangeBackground});
    }
`;

export const MaxWidth = styled.css`
	max-width: 1380px;
    margin-left:auto;
    margin-right:auto;
`;
