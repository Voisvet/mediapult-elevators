import * as React from 'react';

import WelcomeBlock from './WelcomeBlock/WelcomeBlock';
import ArgumentsBlock from './ArgumentsBlock/ArgumentsBlock';
import ScreensBlock from './ScreensBlock/ScreensBlock';
import SystemAdvantagesBlock from './SystemAdvantagesBlock/SystemAdvantagesBlock';
import FooterBlock from './Footer/Footer';
class Landing extends React.Component<any, any> {
    render(): React.ReactNode {
        return (
            <React.Fragment>
                <WelcomeBlock {...this.props}/>
                <ArgumentsBlock/>
                <ScreensBlock/>
                <SystemAdvantagesBlock/>
                <FooterBlock/>
            </React.Fragment>
        );
    }
}

export default Landing;
