import * as React from 'react';
import { Row, Circle , ArgumentItem, Text, ImgRow } from './GridPart.styles';
import { Waypoint } from 'react-waypoint';
import { textForSystemAdvamtages as text } from '../../Strings.styles';
import { H1 as TextContainer } from "../../Landing.styles";

class GridPart extends React.Component<any, any> {
    constructor(props) {
        super(props);
        this.state = {
            isInView: false
        };
        this.onEnter = this.onEnter.bind(this);

    }

    onEnter({ previousPosition }){
        if (previousPosition === Waypoint.below) {
            this.setState({
                isInView: true
            });
        } else {
            this.setState({
                isInView: false
            });
        }
    }
    render(): React.ReactNode {
        return (
            <div>
            <Waypoint onEnter={this.onEnter}/>
            <Row>
                    <ArgumentItem isInViewState={this.state.isInView}>
                        <ImgRow><Circle><TextContainer>1</TextContainer></Circle></ImgRow><TextContainer><Text>{text.t1}</Text></TextContainer>
                    </ArgumentItem>
                    <ArgumentItem isInViewState={this.state.isInView}>
                        <ImgRow><Circle><TextContainer>2</TextContainer></Circle></ImgRow><TextContainer><Text>{text.t2}</Text></TextContainer>
                    </ArgumentItem>
                    <ArgumentItem isInViewState={this.state.isInView}>
                        <ImgRow><Circle><TextContainer>3</TextContainer></Circle></ImgRow><TextContainer><Text>{text.t3}</Text></TextContainer>
                    </ArgumentItem>
                    <ArgumentItem isInViewState={this.state.isInView}>
                        <ImgRow><Circle><TextContainer>4</TextContainer></Circle></ImgRow><TextContainer><Text>{text.t4}</Text></TextContainer>
                    </ArgumentItem>
            </Row>
            </div>
        );
    }
}

export default GridPart;
