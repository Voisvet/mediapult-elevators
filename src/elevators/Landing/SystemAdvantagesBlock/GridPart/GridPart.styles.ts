import * as styled from 'styled-components';
import { TextContainer, H3 } from '../../Landing.styles';
import { smallTopPadding } from '../../Paddings.styles';
import { flipInY } from '../../Animation.styles';

export const Circle = styled(TextContainer)`
	width: 4rem;
    color:white;
    height: 4rem;
    display:grid;
    grid-column:2/3;
    justify-content: center;
    align-items: center;
     margin-bottom: 0.5rem;
        @media (max-width: 991px) {
        width: 3.5rem;
        height: 3.5rem;
    margin-bottom: 1rem;
    }
    border: 0.2vmin solid white;
    border-radius: 50%;
    @media (max-width: 991px) {
        width: 3rem;
        height:3rem;
        border: 0.2vmin solid white;
    }
    @media (max-width: 767px) {
        width: 2.5rem;
        height: 2.5rem;
        border: 0.2vmin solid white;
        margin-bottom: 0.5rem;
    }
`;

export const ArgumentItem = styled.div`
    display:grid;
    align-items: flex-start;
    justify-content: center;
    border-radius: 2ex;
    padding: 2rem
    background-color: rgba(255, 255, 255, 0.22);
    height: 100%;
        @media (min-width:481px) and (max-width: 991px) {
	    padding: 2rem;
	    width:100%;
	}
	 @media (max-width:480px){
	    padding: 1rem;
	    width:100%;
	}
	transition: background-color 0.5s ease;
    ${props => (props.isInViewState ? spinHorizontalAnimation : '')}
`;

const spinHorizontalAnimation = styled.css`
	animation: ${flipInY} 1500ms 1;
`;

export const ImgRow = styled.div`
    display:grid;
    grid-template-columns: repeat(3, 1fr);
    justify-items:center;
    align-items:center;    
`;

export const Row = styled.div`
    ${smallTopPadding};
    display:grid;
    grid-template-columns: 1fr  1fr  1fr  1fr;
    grid-gap:8vmin;
    grid-auto-rows:minmax(100px, auto);
    justify-items:center;
    align-items:start;
    @media (orientation:landscape){
    grid-template-columns: 1fr  1fr  1fr  1fr;
    grid-gap:7vmin;
    }
    @media (max-width: 991px) {
    grid-template-columns: 1fr 1fr;
    grid-gap:6vmin;
    }  
`;
export const Text = styled(H3)`
   width:100%!important; 
`;
