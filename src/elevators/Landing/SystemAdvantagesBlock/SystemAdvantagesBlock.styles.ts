import * as styled from 'styled-components';
import { H2, MaxWidth } from '../Landing.styles';
import { lightBlueColor } from '../Colors.styles';
import { mainPadding, leftRightPadding, topPadding, bottomPadding } from '../Paddings.styles';

export const SystemAdvantagesBlock = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    ${MaxWidth};
    background-image: linear-gradient(to right, ${lightBlueColor} , #1991eb);
    color: #ffffff;
    width: 100%;
    ${leftRightPadding};    
    ${topPadding};
    ${bottomPadding};
`;

export const Header = styled(H2)`
	color:white;
	width:70%;
	/* Portrait tablets and medium desktops */
	@media (min-width: 992px) and (max-width: 1199px) {
	width:80%;
	}
	/* Portrait tablets and small desktops */
	@media (min-width: 768px) and (max-width: 991px) {
	width:90%;
	}
	
	/* Landscape phones and portrait tablets */
	@media (max-width: 767px) {
	width:100%;
	}
`;
