import * as React from 'react';

import { SystemAdvantagesBlock as StyledSystemAdvantagesBlock, Header } from './SystemAdvantagesBlock.styles';
import GridPart from './GridPart/GridPart';
import { H2 as StyledHeader, H1 as TextContainer } from './../Landing.styles';
import { headerForSystemAdvantages as header } from '../Strings.styles';
class SystemAdvantagesBlock extends React.Component<any, any> {
    render(): React.ReactNode {
        return (
            <StyledSystemAdvantagesBlock>
                <TextContainer><Header>{header.h1}</Header>
                {/*<StyledHeader color="white" weight="600">СЕРВИС MEDIAPULT</StyledHeader>*/}
                </TextContainer>
                <GridPart/>
            </StyledSystemAdvantagesBlock>
        );
    }
}

export default SystemAdvantagesBlock;
