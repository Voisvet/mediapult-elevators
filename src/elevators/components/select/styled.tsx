import * as React from 'react';
import * as styled from 'styled-components';

export const Wrapper: any = styled.div`
  user-select: none;
  cursor: pointer;
  position: relative;
  
  .bordered {
    border-color: rgb(46, 162, 248);
  }
`;

export const Option = styled.li`
  list-style-type:none;
  font-family: 'ProximaNovaSemibold';
  cursor: pointer;
  display: block;
  font-size: 14px;
  color: rgb(53, 64, 82);
  height: 40px;
  display: flex;
  align-items: center;
  
  &:hover {
    background-color: #f1f4f8;
  }
  border-bottom: 1px solid rgb(223, 227, 233);

  &:last-child {
    border-bottom: none;
  }
`;

export const OptionButton = styled.button`
  width: 100%;
  height: 100%;
  background: none;
  border: none;
  cursor: pointer;
  display: flex;
  align-items: center;
  
  &:hover {
    color: #2ea2f8;
  }

  > div:first-child{
    float: left;
    padding: 0 16px;
    flex: 1 0 auto;
    text-align: left;
  }
`;

export const SelectedOptionButton = styled(OptionButton)`
  color: #00aeef;
  background-color: #f1f4f8;

  > div:last-child{
    float: left;
    flex: 0 0 auto;
    margin-right: 16px;
    display: flex;
  }
`;

export const OuterList = styled.ul`
  position: absolute;
  border-width: 1px;
  border-color: rgb(46, 162, 248);
  border-style: solid;
  background-color: rgb(255, 255, 255);
  box-shadow: 0px 1px 4px 0px rgba(0, 0, 0, 0.08);
  border-bottom-right-radius: 4px;
  border-bottom-left-radius: 4px;
  min-width: 65px;
  width: 100%;
  z-index: 1;
  top: 37px;
`;

export const Input = styled.button`
  border: 1px solid rgb(206, 208, 218);
  border-radius: 4px;
  background-image: -moz-linear-gradient( 90deg, rgb(242,244,247) 0%, rgb(254,255,255) 100%);
  background-image: -webkit-linear-gradient( 90deg, rgb(242,244,247) 0%, rgb(254,255,255) 100%);
  background-image: -ms-linear-gradient( 90deg, rgb(242,244,247) 0%, rgb(254,255,255) 100%);
  font-size: 14px;
  color: #354052;
  display: inline-flex;
  flex-direction: row;
  align-items: center;
  position: relative;
  width: 100%;
  height: 40px;

  > div:first-child{
    float: left;
    flex: 0 0 auto;
    display: flex;
    margin-left: 16px;
  }

  > div:nth-child(2){
    float: left;
    padding: 0 8px;
    flex: 1 0 auto;
    text-align: left;
  }

  > div:last-child{
    float: left;
    flex: 0 0 auto;
    display: flex;
    margin-right: 16px;
  }

  &:focus {
    outline: none;
  }
`;

const SmallSVG = styled.svg`
  width: 16px;
  height: 16px;
`;

export const DropDownIcon = (props: any) => {
    return (
    <SmallSVG {...props} viewBox="0 0 24 24">
        <path
            d="M7.41 8.59L12 13.17l4.59-4.58L18 10l-6 6-6-6 1.41-1.41z"
            fill-rule="evenodd"
            fill="rgb(168, 170, 183)"
        />
        <path fill="none" d="M0 0h24v24H0V0z"/>
   </SmallSVG>
    );
};

export const DropUpIcon = (props: any) => {
    return (
        <SmallSVG {...props} viewBox="0 0 24 24">
            <path
                d="M7.41 15.41L12 10.83l4.59 4.58L18 14l-6-6-6 6z"
                fill-rule="evenodd"
                fill="rgb(168, 170, 183)"
            />
            <path fill="none" d="M0 0h24v24H0V0z"/>
        </SmallSVG>
    );
};

export const CheckIcon = (props: any) => {
    return (
        <SmallSVG {...props} viewBox="0 0 24 24">
            <path d="M0 0h24v24H0z" fill="none"/>
            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z" fill="#00aeef"/>
        </SmallSVG>
    );
};

const BigSVG = styled.svg`
  width: 16px;
  height: 16px;
`;

export const MapIcon = (props: any) => {
    return (
        <BigSVG {...props} viewBox="0 0 24 24">
            <path
                d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38
                0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z"
                fill-rule="evenodd"
                fill="rgb(168, 170, 183)"
            />
            <path d="M0 0h24v24H0z" fill="none"/>
        </BigSVG>
    )
}
