import * as React from 'react';

import {
    Option,
    OptionButton,
    SelectedOptionButton,
    OuterList,
    Input,
    DropDownIcon,
    DropUpIcon,
    CheckIcon,
    MapIcon,
    Wrapper
} from './styled';

export default class Select extends React.Component<any, any> {
    propsOptions: any = [];
    wrapperRef: any;

    constructor(props: any) {
        super(props);
        this.toogleOuterList = this.toogleOuterList.bind(this);
        this.selected = this.selected.bind(this);

        if (props.range) {
            for (let i = props.range.start; i <= props.range.end; i++) {
                this.propsOptions.push({label: i, value: i});
            }
        }
        this.propsOptions = this.props.options
            ? this.props.options
            : this.propsOptions;

        let defState = {
            selectedValue: props.value ? props.value.value : this.propsOptions[0].value,
            selectedLabel: props.value ? props.value.label : this.propsOptions[0].label,
            show: false
        };
        this.state = {...this.state, ...defState};
    }

    componentDidMount() {
        document.addEventListener('mousedown', this.handleClickOutside);
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside);
    }

    handleClickOutside = (event) => {
        if (this.wrapperRef && !this.wrapperRef.contains(event.target) && this.state.show) {
            this.setState({show: false});
        }
    };

    setWrapperRef = (node) => {
        this.wrapperRef = node;
    };

    selected(e: any, item: any) {
        this.setState({
                          selectedValue: item.value,
                          selectedLabel: item.label
                      });
        this.props.onChange(item);
    }

    toogleOuterList(value: any) {
        this.setState({show: !value});
    }

    render() {
        let labelToRender;
        if (this.props.value) {
            labelToRender = this.props.value.label;
        } else {
            labelToRender = this.state.selectedLabel;
        }

        return (
            <Wrapper {...this.props} value={this.state.selectedValue} ref={this.setWrapperRef}>
                <Input
                    className={this.state.show ? 'bordered' : ''}
                    type="button"
                    // onBlur={this.hide}
                    onClick={() => this.toogleOuterList(this.state.show)}
                >
                    <div><MapIcon/></div>
                    <div>{labelToRender}</div>
                    <div>
                        {this.state.show ? (<DropUpIcon/>) : (<DropDownIcon/>)}
                    </div>
                </Input>
                {this.state !== null &&
                this.state.show && <OuterList>{
                    this.propsOptions.map((item, index) => {
                        return (
                            <Option key={index} value={item.value}>
                                {item.value === this.state.selectedValue ? (
                                    <SelectedOptionButton
                                        onClick={() => this.selected(event, item)}
                                    >
                                        <div>{item.label}</div>
                                        <div><CheckIcon/></div>
                                    </SelectedOptionButton>
                                ) : (
                                    <OptionButton onClick={() => this.selected(event, item)}>
                                        <div>{item.label}</div>
                                    </OptionButton>
                                )}
                            </Option>
                        );
                    })
                }</OuterList>}
            </Wrapper>
        );
    }
}
