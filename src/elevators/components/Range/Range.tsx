import * as React from 'react';
import * as Slider from 'rc-slider';

// import { createSliderWithTooltip } from './createSliderWithTooltip';

export const Range = (props: {min: number, max: number, value?: number[], onAfterChange?: any}) => {
    const RangeWithTooltips = Slider.createSliderWithTooltip(Slider.Range);
    const { min, max, value, onAfterChange } = props;

    const marks = {};

    let i, position;
    for (i = 1; i < 4; i++) {
        position = Math.round(min + (max - min) / 4 * i);
        marks[position] = {
            label: position
        };
    }
    marks[min] = {
        label: min,
        style: {
            left: '3%'
        }
    };
    marks[max] = {
        label: max,
        style: {
            left: '97%'
        }
    };

    return (
        <RangeWithTooltips
            min={min}
            max={max}
            pushable={(max - min) * 0.03}
            marks={marks}
            defaultValue={value}
            onAfterChange={onAfterChange}
            handleStyle={[
                {
                    backgroundColor: 'white',
                    borderTopLeftRadius: '5px',
                    borderTopRightRadius: '0',
                    borderBottomLeftRadius: '5px',
                    borderBottomRightRadius: '0',
                    border: '2px solid rgb(220, 220, 220)',
                    width: '10px',
                    height: '16px',
                    marginTop: '-6px'
                },
                {
                    backgroundColor: 'white',
                    borderTopLeftRadius: '0',
                    borderTopRightRadius: '5px',
                    borderBottomLeftRadius: '0',
                    borderBottomRightRadius: '5px',
                    border: '2px solid rgb(220, 220, 220)',
                    width: '10px',
                    height: '16px',
                    marginTop: '-6px'
                }
            ]}
            trackStyle={[{
                backgroundColor: '#00aeef'
            }]}
            dotStyle={{
                display: 'none'
            }}
        />
    );
};

