import * as React from 'react';
import { connect } from 'react-redux';

import Landing from './Landing/Landing';
import Order from './Order/Order';

import { GlobalStyle } from './index.styles';

const processes = {
    elevatorFlow: {
        elevatorStart: Landing,
        elevatorOrder1: Order
    }
};

class Main extends React.PureComponent <any, any> {
    constructor(props: any) {
        super(props);
        props.initFlow({
                           flowName: 'elevatorFlow',
                           url: 'elevators-bh'
                       });
    }

    componentDidMount(): void {
        this.props.initFlow({
                                flowName: 'elevatorFlow',
                                url: 'elevators-bh'
                            });
    }

    render() {
        const {flowName, stateName, isLoading} = this.props;

        if (isLoading) {
            return null;
        }

        const Page = (flowName && flowName === 'elevatorFlow' && stateName) ? processes[flowName][stateName] : '';

        return (
            <React.Suspense fallback={<div>Loading</div>}>
                <GlobalStyle/>
                {Page && <Page {...this.props} />}
            </React.Suspense>
        );
    }
}

const mapStateToProps = (state) => ({});

const mapDispatchToProps = (dispatch) => ({});

const connectedApp = connect(mapStateToProps, mapDispatchToProps)(Main);
const reducers = 'reducer';
connectedApp[reducers] = (state = {exampleProperty: '111'}, action) => state;

export default connectedApp;
